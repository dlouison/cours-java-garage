package com.iia.shop.view;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.iia.shop.dao.VehicleDAO;
import com.iia.shop.entity.Vehicle;
import java.awt.Color;

public class Garage {

	private JFrame frame;

	private VehicleDAO vehicleDao;
	private ArrayList<Vehicle> vehicles;

	private JList<Object> listVehicles;

	private JTextField txtVehicleBrand;
	private JTextField txtVehicleModel;
	private JTextField txtVehicleColor;
	private JTextField txtVehicleYear;

	private JButton btnAddVehicle;
	private JButton btnUpdateVehicle;
	private JButton btnDeleteVehicle;
	private JButton btnCancelUpdateVehicle;
	private JTextField txtVehiclePrice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Garage window = new Garage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Garage() {
		initialize();

		this.vehicleDao = new VehicleDAO();

		this.getOrRefreshVehicleList();

		this.listVehicles.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!Garage.this.listVehicles.isSelectionEmpty())
					Garage.this.setVehicleInfos((Vehicle) Garage.this.listVehicles.getSelectedValue());
			}
		});

		this.btnAddVehicle.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				NewVehicle newVehicle = new NewVehicle();
				
				newVehicle.setVisible(true);
				newVehicle.addWindowListener(new WindowAdapter() {

					@Override
					public void windowClosed(WindowEvent e) {
						if (newVehicle.isCreated){
							Garage.this.getOrRefreshVehicleList();
						}
					}
					
				});
			}
		});
		this.btnDeleteVehicle.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Vehicle selectedVehicle = (Vehicle) Garage.this.listVehicles.getSelectedValue();
				Garage.this.deleteVehicle(selectedVehicle);

			}
		});

		this.btnUpdateVehicle.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!Garage.this.listVehicles.isSelectionEmpty()){
					Vehicle selectedVehicle = (Vehicle) Garage.this.listVehicles.getSelectedValue();
	
					selectedVehicle.setBrand(Garage.this.txtVehicleBrand.getText());
					selectedVehicle.setModel(Garage.this.txtVehicleModel.getText());
					selectedVehicle.setYear(Integer.parseInt(Garage.this.txtVehicleYear.getText()));
					selectedVehicle.setPrice(Float.parseFloat(Garage.this.txtVehiclePrice.getText()));
					selectedVehicle.setColor(Garage.this.txtVehicleColor.getText());
	
					Garage.this.vehicleDao.update(selectedVehicle);
	
					Garage.this.getOrRefreshVehicleList();
				}
			}
		});

		this.listVehicles.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_DELETE){
					Vehicle selectedVehicle = (Vehicle) Garage.this.listVehicles.getSelectedValue();
					Garage.this.deleteVehicle(selectedVehicle);
				}
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel northPanel = new JPanel();
		frame.getContentPane().add(northPanel, BorderLayout.NORTH);

		JLabel lblGarage = new JLabel("Garage");
		lblGarage.setFont(UIManager.getFont("OptionPane.font"));
		northPanel.add(lblGarage);

		JPanel westPanel = new JPanel();
		westPanel.setForeground(UIManager.getColor("Button.foreground"));
		frame.getContentPane().add(westPanel, BorderLayout.WEST);
		westPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		westPanel.add(splitPane);

		JSplitPane splitPane_1 = new JSplitPane();
		splitPane.setRightComponent(splitPane_1);

		listVehicles = new JList<Object>();
		listVehicles.setFont(UIManager.getFont("ColorChooser.font"));
		listVehicles.setVisibleRowCount(10);
		listVehicles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		splitPane.setLeftComponent(listVehicles);

		btnDeleteVehicle = new JButton("Supprimer");
		btnDeleteVehicle.setBackground(UIManager.getColor("Button.disabledToolBarBorderBackground"));
		btnDeleteVehicle.setFont(UIManager.getFont("ToolTip.font"));
		btnDeleteVehicle.setForeground(UIManager.getColor("Button.foreground"));
		splitPane_1.setLeftComponent(btnDeleteVehicle);

		btnAddVehicle = new JButton("Ajouter");
		btnAddVehicle.setFont(UIManager.getFont("ToolTip.font"));
		btnAddVehicle.setForeground(UIManager.getColor("Button.foreground"));
		splitPane_1.setRightComponent(btnAddVehicle);

		JPanel centerPanel = new JPanel();
		frame.getContentPane().add(centerPanel, BorderLayout.CENTER);

		JSplitPane splitPane_2 = new JSplitPane();
		splitPane_2.setOrientation(JSplitPane.VERTICAL_SPLIT);
		centerPanel.add(splitPane_2);

		JSplitPane splitPane_3 = new JSplitPane();
		splitPane_2.setRightComponent(splitPane_3);

		btnUpdateVehicle = new JButton("Modifier");
		btnUpdateVehicle.setFont(UIManager.getFont("ToolTip.font"));
		btnUpdateVehicle.setForeground(UIManager.getColor("Button.foreground"));
		splitPane_3.setLeftComponent(btnUpdateVehicle);

		btnCancelUpdateVehicle = new JButton("Annuler");
		btnCancelUpdateVehicle.setForeground(UIManager.getColor("Button.foreground"));
		btnCancelUpdateVehicle.setFont(UIManager.getFont("ToolTip.font"));
		splitPane_3.setRightComponent(btnCancelUpdateVehicle);

		JPanel panel = new JPanel();
		splitPane_2.setLeftComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblVehicleBrand = new JLabel("Marque :");
		GridBagConstraints gbc_lblVehicleBrand = new GridBagConstraints();
		gbc_lblVehicleBrand.insets = new Insets(0, 0, 5, 0);
		gbc_lblVehicleBrand.gridx = 0;
		gbc_lblVehicleBrand.gridy = 0;
		panel.add(lblVehicleBrand, gbc_lblVehicleBrand);

		txtVehicleBrand = new JTextField();
		GridBagConstraints gbc_txtVehicleBrand = new GridBagConstraints();
		gbc_txtVehicleBrand.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehicleBrand.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehicleBrand.gridx = 0;
		gbc_txtVehicleBrand.gridy = 1;
		panel.add(txtVehicleBrand, gbc_txtVehicleBrand);
		txtVehicleBrand.setColumns(10);

		JLabel lblVehicleModel = new JLabel("Modèle :");
		GridBagConstraints gbc_lblVehicleModel = new GridBagConstraints();
		gbc_lblVehicleModel.insets = new Insets(0, 0, 5, 0);
		gbc_lblVehicleModel.gridx = 0;
		gbc_lblVehicleModel.gridy = 2;
		panel.add(lblVehicleModel, gbc_lblVehicleModel);

		txtVehicleModel = new JTextField();
		txtVehicleModel.setColumns(10);
		GridBagConstraints gbc_txtVehicleModel = new GridBagConstraints();
		gbc_txtVehicleModel.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehicleModel.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehicleModel.gridx = 0;
		gbc_txtVehicleModel.gridy = 3;
		panel.add(txtVehicleModel, gbc_txtVehicleModel);

		JLabel labelVehicleColor = new JLabel("Couleur :");
		GridBagConstraints gbc_labelVehicleColor = new GridBagConstraints();
		gbc_labelVehicleColor.insets = new Insets(0, 0, 5, 0);
		gbc_labelVehicleColor.gridx = 0;
		gbc_labelVehicleColor.gridy = 4;
		panel.add(labelVehicleColor, gbc_labelVehicleColor);

		txtVehicleColor = new JTextField();
		txtVehicleColor.setColumns(10);
		GridBagConstraints gbc_txtVehicleColor = new GridBagConstraints();
		gbc_txtVehicleColor.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehicleColor.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehicleColor.gridx = 0;
		gbc_txtVehicleColor.gridy = 5;
		panel.add(txtVehicleColor, gbc_txtVehicleColor);

		JLabel labelVehicleYear = new JLabel("Année :");
		GridBagConstraints gbc_labelVehicleYear = new GridBagConstraints();
		gbc_labelVehicleYear.insets = new Insets(0, 0, 5, 0);
		gbc_labelVehicleYear.gridx = 0;
		gbc_labelVehicleYear.gridy = 6;
		panel.add(labelVehicleYear, gbc_labelVehicleYear);

		txtVehicleYear = new JTextField();
		txtVehicleYear.setColumns(10);
		GridBagConstraints gbc_txtVehicleYear = new GridBagConstraints();
		gbc_txtVehicleYear.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehicleYear.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehicleYear.gridx = 0;
		gbc_txtVehicleYear.gridy = 7;
		panel.add(txtVehicleYear, gbc_txtVehicleYear);

		JLabel labelVehiclePrice = new JLabel("Prix :");
		GridBagConstraints gbc_labelVehiclePrice = new GridBagConstraints();
		gbc_labelVehiclePrice.insets = new Insets(0, 0, 5, 0);
		gbc_labelVehiclePrice.gridx = 0;
		gbc_labelVehiclePrice.gridy = 8;
		panel.add(labelVehiclePrice, gbc_labelVehiclePrice);

		txtVehiclePrice = new JTextField();
		txtVehiclePrice.setColumns(10);
		GridBagConstraints gbc_txtVehiclePrice = new GridBagConstraints();
		gbc_txtVehiclePrice.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehiclePrice.gridx = 0;
		gbc_txtVehiclePrice.gridy = 9;
		panel.add(txtVehiclePrice, gbc_txtVehiclePrice);
	}

	private void getOrRefreshVehicleList() {
		this.vehicles = (ArrayList<Vehicle>) vehicleDao.findAll();
		this.listVehicles.setListData(vehicles.toArray());
	}

	private void setVehicleInfos(Vehicle vehicle) {
		Garage.this.txtVehicleBrand.setText(vehicle.getBrand());
		Garage.this.txtVehicleModel.setText(vehicle.getModel());
		Garage.this.txtVehicleColor.setText(vehicle.getColor());
		Garage.this.txtVehicleYear.setText(String.valueOf(vehicle.getYear()));
		Garage.this.txtVehiclePrice.setText(String.valueOf(vehicle.getPrice()));
	}
	
	private void deleteVehicle(Vehicle vehicle){
		int deleteConfirmation = JOptionPane.showConfirmDialog(null,
				"Voulez-vous vraiment supprimer le véhicule ?", "Suppression d'un véhicule",
				JOptionPane.YES_NO_OPTION);

		if (deleteConfirmation == JOptionPane.YES_OPTION) {
			// Get current index
			int currentIndex = Garage.this.listVehicles.getSelectedIndex();

			Garage.this.vehicleDao.delete(vehicle);
			Garage.this.getOrRefreshVehicleList();
		}
	}
}
