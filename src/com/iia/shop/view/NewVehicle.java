package com.iia.shop.view;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.iia.shop.dao.VehicleDAO;
import com.iia.shop.entity.Vehicle;

public class NewVehicle extends JFrame {

	private JPanel contentPane;
	private JTextField txtVehicleBrand;
	private JTextField txtVehicleModel;
	private JTextField txtVehicleColor;
	private JTextField txtVehiclePrice;
	private JTextField txtVehicleYear;

	private VehicleDAO vehicleDao;
	
	boolean isCreated;
	/**
	 * Create the frame.
	 */
	public NewVehicle() {
		this.vehicleDao = new VehicleDAO();
		isCreated = false;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.SOUTH);
		
		JButton btnSaveAddVehicle = new JButton("Enregistrer");
		splitPane.setLeftComponent(btnSaveAddVehicle);
		
		JButton btnCancelAddVehicle = new JButton("Annuler");
		splitPane.setRightComponent(btnCancelAddVehicle);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{137, 35, 232, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblMarque = new JLabel("Marque :");
		GridBagConstraints gbc_lblMarque = new GridBagConstraints();
		gbc_lblMarque.insets = new Insets(0, 0, 5, 5);
		gbc_lblMarque.gridx = 0;
		gbc_lblMarque.gridy = 1;
		panel.add(lblMarque, gbc_lblMarque);
		
		txtVehicleBrand = new JTextField();
		GridBagConstraints gbc_txtVehicleBrand = new GridBagConstraints();
		gbc_txtVehicleBrand.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehicleBrand.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehicleBrand.gridx = 2;
		gbc_txtVehicleBrand.gridy = 1;
		panel.add(txtVehicleBrand, gbc_txtVehicleBrand);
		txtVehicleBrand.setColumns(10);
		
		JLabel lblVehicleModel = new JLabel("Modèle :");
		GridBagConstraints gbc_lblVehicleModel = new GridBagConstraints();
		gbc_lblVehicleModel.insets = new Insets(0, 0, 5, 5);
		gbc_lblVehicleModel.gridx = 0;
		gbc_lblVehicleModel.gridy = 3;
		panel.add(lblVehicleModel, gbc_lblVehicleModel);
		
		txtVehicleModel = new JTextField();
		GridBagConstraints gbc_txtVehicleModel = new GridBagConstraints();
		gbc_txtVehicleModel.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehicleModel.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehicleModel.gridx = 2;
		gbc_txtVehicleModel.gridy = 3;
		panel.add(txtVehicleModel, gbc_txtVehicleModel);
		txtVehicleModel.setColumns(10);
		
		JLabel lblVehicleColor = new JLabel("Couleur :");
		GridBagConstraints gbc_lblVehicleColor = new GridBagConstraints();
		gbc_lblVehicleColor.insets = new Insets(0, 0, 5, 5);
		gbc_lblVehicleColor.gridx = 0;
		gbc_lblVehicleColor.gridy = 5;
		panel.add(lblVehicleColor, gbc_lblVehicleColor);
		
		txtVehicleColor = new JTextField();
		GridBagConstraints gbc_txtVehicleColor = new GridBagConstraints();
		gbc_txtVehicleColor.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehicleColor.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehicleColor.gridx = 2;
		gbc_txtVehicleColor.gridy = 5;
		panel.add(txtVehicleColor, gbc_txtVehicleColor);
		txtVehicleColor.setColumns(10);
		
		JLabel lblVehiclePrice = new JLabel("Prix :");
		GridBagConstraints gbc_lblVehiclePrice = new GridBagConstraints();
		gbc_lblVehiclePrice.insets = new Insets(0, 0, 5, 5);
		gbc_lblVehiclePrice.gridx = 0;
		gbc_lblVehiclePrice.gridy = 7;
		panel.add(lblVehiclePrice, gbc_lblVehiclePrice);
		
		txtVehiclePrice = new JTextField();
		GridBagConstraints gbc_txtVehiclePrice = new GridBagConstraints();
		gbc_txtVehiclePrice.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehiclePrice.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehiclePrice.gridx = 2;
		gbc_txtVehiclePrice.gridy = 7;
		panel.add(txtVehiclePrice, gbc_txtVehiclePrice);
		txtVehiclePrice.setColumns(10);
		
		JLabel labelVehicleYear = new JLabel("Année :");
		GridBagConstraints gbc_labelVehicleYear = new GridBagConstraints();
		gbc_labelVehicleYear.insets = new Insets(0, 0, 5, 5);
		gbc_labelVehicleYear.gridx = 0;
		gbc_labelVehicleYear.gridy = 9;
		panel.add(labelVehicleYear, gbc_labelVehicleYear);
		
		txtVehicleYear = new JTextField();
		GridBagConstraints gbc_txtVehicleYear = new GridBagConstraints();
		gbc_txtVehicleYear.insets = new Insets(0, 0, 5, 0);
		gbc_txtVehicleYear.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVehicleYear.gridx = 2;
		gbc_txtVehicleYear.gridy = 9;
		panel.add(txtVehicleYear, gbc_txtVehicleYear);
		txtVehicleYear.setColumns(10);
		
		
		
		btnSaveAddVehicle.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Vehicle vehicle = new Vehicle();
				
				String brand, model, color;
				int year;
				float price;
				
				brand = NewVehicle.this.txtVehicleBrand.getText();
				model = NewVehicle.this.txtVehicleModel.getText();
				color = NewVehicle.this.txtVehicleColor.getText();
				

				if (!brand.isEmpty() && !model.isEmpty() && !color.isEmpty()){
					if(	!NewVehicle.this.txtVehicleYear.getText().isEmpty() 
						&& 
						!NewVehicle.this.txtVehiclePrice.getText().isEmpty()){
						try{
							year = Integer.parseInt(NewVehicle.this.txtVehicleYear.getText());
							price = Float.parseFloat(NewVehicle.this.txtVehiclePrice.getText());

							vehicle.setBrand(brand);
							vehicle.setModel(model);
							vehicle.setColor(color);
							vehicle.setPrice(price);
							vehicle.setYear(year);
							
							NewVehicle.this.isCreated = vehicleDao.create(vehicle);

						}
						catch (Exception e1) {
							System.out.println(e1.getMessage());
						}
					}
						

				}


				
				NewVehicle.this.dispose();
			}
		});
		
		btnCancelAddVehicle.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				NewVehicle.this.dispose();
			}
		});
	}
	
	

}
