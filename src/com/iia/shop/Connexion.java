package com.iia.shop;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

public class Connexion {
	private static final String URL = "jdbc:mysql://localhost:3306/Garage?useSSL=false";
	private static final String PASSWORD = "louison";
	private static final String LOGIN = "louison";
	
	private static Connection connection;
	
	public static Connection getConnection(){
		Connexion.connection = null;
		try {
			// Loading mysql driver
			Class.forName("com.mysql.jdbc.Driver");
			
			Connexion.connection = DriverManager.getConnection(
					Connexion.URL, Connexion.LOGIN, Connexion.PASSWORD);
			
			return Connexion.connection;
			
			
		} catch (ClassNotFoundException e) {
			System.out.println("Impossible de charger le driver");
		} catch (SQLException e) {
			System.out.println("Erreur lors de la connexion");
		}
		
		
		return null;
	}
	
	public static void close(){
		try {
			Connexion.connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
