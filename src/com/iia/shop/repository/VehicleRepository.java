package com.iia.shop.repository;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.iia.shop.entity.Vehicle;

public class VehicleRepository {
	private static File saving_folder = new File("/home/louison/Documents/Cours/Java");
	private static File vehicles_saving_file = new File(saving_folder, "shop_vehicles");
	private static File vehicles_saving_file_txt = new File (saving_folder, "shop_vehicles_txt");
	
	public static void saveTxt(ArrayList<Vehicle> vehicles, boolean keepPrevious){
		try {
			FileWriter writer = new FileWriter(vehicles_saving_file_txt, keepPrevious);
			
			BufferedWriter buffer = new BufferedWriter(writer);
			
			for (Vehicle vehicle : vehicles) {
				buffer.write(vehicle.toString());
				buffer.newLine();
			}

			
			buffer.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	public static ArrayList<Vehicle> getAllFromTxt(){
		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

		try {
			FileReader reader = new FileReader(vehicles_saving_file_txt);
			BufferedReader buffer = new BufferedReader(reader);
			

			while (buffer.ready()) {
				String[] vehicleInfos = buffer.readLine().split(",");
				Vehicle vehicleTemp = new Vehicle(
						vehicleInfos[0], 
						vehicleInfos[1],
						vehicleInfos[2], 
						Integer.parseInt(vehicleInfos[3]),
						Double.parseDouble(vehicleInfos[5]));
				vehicles.add(vehicleTemp);
			}
			
			buffer.close();
			reader.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return vehicles;
	}
	
	public static void save(ArrayList<Vehicle> vehicles, boolean keepPrevious){
		try {
			FileOutputStream outStream = new FileOutputStream(vehicles_saving_file, keepPrevious);
			
			ObjectOutputStream objectOutput = new ObjectOutputStream(outStream);
			
			objectOutput.writeObject(vehicles);

			objectOutput.close();
			outStream.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Remove warnings on (ArrayList<Vehicle>) cast
	@SuppressWarnings({ "unchecked" })
	public static ArrayList<Vehicle> getAll(){
		FileInputStream inputStream;
		
		if (vehicles_saving_file.exists()){
			
			try {
				inputStream = new FileInputStream(vehicles_saving_file);
				ObjectInputStream objectIn = new ObjectInputStream(inputStream);
	
				ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
				
				try {
					vehicles = (ArrayList<Vehicle>)objectIn.readObject();
				
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (EOFException e){
					e.printStackTrace();
				}
				
				objectIn.close();
				inputStream.close();
				
				return vehicles;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
		
	}

}
