package com.iia.shop.entity;

public class Vehicle implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int identifiant;
	private String brand;
	private String model;
	private String color;
	private int year;
	private int speed;
	private double price;
	private boolean isStarted;
	
	public Vehicle() {
		this(null, null, null, 0, 0);
	}
	
	public Vehicle(String brand, String model, String color, int year, double price) {
		this(-1, brand, model, color, year, price);
	}
	
	public Vehicle(int identifiant, String brand, String model, String color, int year, double price) {
		super();
		this.brand = brand;
		this.model = model;
		this.color = color;
		this.year = year;
		this.speed = 0;
		this.price = price;
		this.isStarted = false;
		this.identifiant = identifiant;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public boolean getIsStarted() {
		return isStarted;
	}
	public void setIsStarted(boolean isStarted) {
		this.isStarted = isStarted;
	}
	
	public void start(){
		this.setIsStarted(true);
		System.out.println("La voiture a démarré");
	}
	
	public void stop(){
		this.setIsStarted(false);
		this.setSpeed(0);
		System.out.println("Le contact a été coupé");
	}
	
	public void speedUp(int speed){
		this.setSpeed(this.getSpeed() + speed);
	}
	
	public void speedDown(int speed){
		if(this.getSpeed() - speed > 0){
			this.setSpeed(this.getSpeed() - speed);
		}
		else{
			this.setSpeed(0);
		}
	}

//	@Override
//	public String toString() {
//		return brand + "," + model + "," + color + "," + year + ","
//				+ speed + "," + price + "," + isStarted;
//	}
	
	@Override
	public String toString() {
		return brand + " " + model + " - " + year;
	}

	public int getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}
	
	
}
