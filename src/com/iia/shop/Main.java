package com.iia.shop;

import java.util.ArrayList;
import java.util.Scanner;

import com.iia.shop.dao.VehicleDAO;
import com.iia.shop.entity.Vehicle;
import com.iia.shop.repository.VehicleRepository;
import com.iia.shop.Connexion;

public class Main {
	private static Scanner sc;
	private static ArrayList<Vehicle> vehicles;
	private static VehicleDAO vehicleDao;

	public static void main(String[] args) {
//		sc = new Scanner(System.in);
//
//		vehicleDao = new VehicleDAO();
//
//		int choice = 0;
//		
//		System.out.println("Bienvenue dans votre garage ! Comment souhaitez vous charger les véhicules ?");
//		
//		choice = (int)getUserChoice(1,3, "1 - Base de donnée \n2 - Txt \n3 - Sérialisé \nChoix : ");
//		
//		// Retrieved saved data
//		switch (choice) {
//		case 2:
//			vehicles = VehicleRepository.getAllFromTxt();
//			break;
//		case 3:
//			vehicles = VehicleRepository.getAll();
//		default:
//			vehicles = (ArrayList<Vehicle>)vehicleDao.findAll();
//			break;
//		}
//				
//		if (vehicles == null){
//			vehicles = new ArrayList<Vehicle>();
//		}
//		
//		menu();
//		
//		sc.close();
//		Connexion.close();
	}

	private static void menu(){
		int choice = 1;

		while(choice != 0){
			System.out.println("-- MENU --");
			System.out.println("1 - Liste des véhicules");
			System.out.println("2 - Ajouter un véhicule");
			System.out.println("3 - Enregistrer les modifications (sérialisé)");
			System.out.println("4 - Enregistrer les modifications (.txt)");

			choice = (int)(getUserChoice(0, 4));
			
			switch (choice) {
				case 1:
					listVehicles();
					break;
				case 2:
					addVehicle();
					VehicleRepository.save(vehicles, false);
					VehicleRepository.saveTxt(vehicles, false);

					menu();
					break;
				case 3:
					// Save without saving old data
					VehicleRepository.save(vehicles, false);
					menu();
					break;
				case 4:
					// Save in txt without saving old data
					VehicleRepository.saveTxt(vehicles, false);

				default:
					break;
			}
		}
	}
	
	private static void addVehicle() {
		String brand, model, color;
		int year;
		double price;
		
		sc.nextLine();

		System.out.println("Marque : ");
		brand = sc.nextLine();
		System.out.println("Modèle : ");
		model = sc.nextLine();
		System.out.println("Couleur : ");
		color = sc.nextLine();

		System.out.print("Année de sortie : ");
		year = sc.nextInt();

		System.out.print("Prix : ");
		price = sc.nextDouble();
		
		Vehicle vehicle = new Vehicle(brand, model, color, year, price);
		
		vehicles.add(vehicle);
		vehicleDao.create(vehicle);
	}

	private static void listVehicles() {
		int choosenVehicle = 0;
		
		if (vehicles != null && vehicles.size() > 0){
			
			System.out.println("0 - Menu principal");
			
			// Show all vehicles in a list
			vehicles.forEach((Vehicle vehicle)->{
				System.out.println(vehicles.indexOf(vehicle)+1 + " - " + vehicle.getBrand() + " " + vehicle.getModel());
			});
			do{
				System.out.print("Choix du véhicule : ");
				choosenVehicle = sc.nextInt();
			}while(choosenVehicle < 0 || choosenVehicle > vehicles.size());

			if(choosenVehicle == 0){
				menu();
			}
			else{
				// choosenVehicle - 1 is the index of the choosen vehicle
				menuVehicle(choosenVehicle-1);
			}
		}
		else{
			System.out.println("Aucun véhicule n'est enregistré pour le moment.");
			menu();
		}
	}
	
	private static void menuVehicle(int indexVehicle){
		int choice = 0;
		
		System.out.println("-- MENU DU VEHICULE --");
		System.out.println("0 - Menu principal");
		System.out.println("1 - Supprimer le véhicule");
		System.out.println("2 - Modifier le véhicule");
		
		choice = getUserChoice(0, 2);
		
		switch (choice) {
		case 1:
			deleteVehicle(indexVehicle);
			menu();
			break;
		case 2:
			updateVehicle(indexVehicle);
			menu();
			break;
		default:
			menu();
			break;
		}
		
		
	}
	
	public static void deleteVehicle(int indexVehicle){
		short confirmation = 0;
		confirmation = (short)(getUserChoice(0, 1, "Voulez vous supprimer (1 oui / 0 non)"));
		
		if (confirmation == 1){
			vehicles.remove(indexVehicle);

			vehicleDao.delete(vehicles.get(indexVehicle));
			System.out.println("Le véhicule a été supprimé\n\n");
		}
	}
	
	private static void updateVehicle(int indexVehicle){
		Vehicle vehicle = vehicles.get(indexVehicle);
		
		String brand = vehicle.getBrand();
		String model = vehicle.getModel();
		String color = vehicle.getColor();
		int year = vehicle.getYear();
		double price = vehicle.getPrice();
		
		sc.nextLine();

		System.out.print("Marque (" + brand + "): ");
		brand = sc.nextLine();
		System.out.print("Modèle (" + model + "): ");
		model = sc.nextLine();
		System.out.print("Couleur (" + color + "): ");
		color = sc.nextLine();

		System.out.print("Année de sortie (" + year + "): ");
		String strYear = sc.next();
		System.out.print("Prix (" + price + "): ");
		String strPrice = sc.next();

		
		if(!brand.isEmpty()){
			vehicle.setBrand(brand);
		}
		if(!model.isEmpty()){
			vehicle.setModel(model);
		}
		if(!color.isEmpty()){
			vehicle.setColor(color);
		}
		if(!strYear.isEmpty()){
			vehicle.setYear(Integer.parseInt(strYear));
		}
		if(!strPrice.isEmpty()){
			vehicle.setPrice(Double.parseDouble(strPrice));
		}
		vehicleDao.update(vehicle);
	}
	
	private static int getUserChoice(int min, int max){
		return getUserChoice(min, max, "Choix : ");
	}
	
	private static int getUserChoice(int min, int max, String message){
		int choice = 0;
		do{
			System.out.print(message);
			choice = sc.nextInt();
		}while(choice < min || choice > max);
		
		return choice;
	}
}
