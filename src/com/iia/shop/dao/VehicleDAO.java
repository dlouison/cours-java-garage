package com.iia.shop.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.iia.shop.Connexion;
import com.iia.shop.entity.Vehicle;

public class VehicleDAO implements IDao<Vehicle> {
	private static final String TABLE_NAME = "Vehicle";
	private static final String ID = "Identifiant";
	private static final String BRAND = "Brand";
	private static final String MODEL = "Model";
	private static final String COLOR = "Color";
	private static final String YEAR = "Year";
	private static final String SPEED = "Speed";
	private static final String PRICE = "Price";
	
	@Override
	public boolean create(Vehicle object) {
		String req = String.format(
				"INSERT INTO %s "
				+ "(%s, %s, %s, %s, %s, %s) "
				+ "VALUES (?, ?, ?, ?, ?, ?)", 
				VehicleDAO.TABLE_NAME, 
				VehicleDAO.BRAND, VehicleDAO.MODEL, VehicleDAO.COLOR, VehicleDAO.YEAR, VehicleDAO.SPEED, VehicleDAO.PRICE);
	
		try {
			PreparedStatement st = Connexion.getConnection().prepareStatement(req);
			
			st.setString(1, object.getBrand());
			st.setString(2, object.getModel());
			st.setString(3, object.getColor());
			st.setInt(4, object.getYear());
			st.setInt(5, object.getSpeed());
			st.setDouble(6, object.getPrice());
			
			if(st.executeUpdate() == 1){
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
	@Override
	public boolean update(Vehicle object) {
		String req = String.format("UPDATE %s "
				+ "SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? "
				+ "WHERE %s = ?",
				VehicleDAO.TABLE_NAME,
				VehicleDAO.BRAND, VehicleDAO.MODEL, VehicleDAO.COLOR, VehicleDAO.YEAR, VehicleDAO.SPEED, VehicleDAO.PRICE,
				VehicleDAO.ID);
		
		try {
			PreparedStatement st = Connexion.getConnection().prepareStatement(req);
			
			st.setString(1, object.getBrand());
			st.setString(2, object.getModel());
			st.setString(3, object.getColor());
			st.setInt(4, object.getYear());
			st.setInt(5, object.getSpeed());
			st.setDouble(6, object.getPrice());
			st.setInt(7, object.getIdentifiant());
			
			if(st.executeUpdate() == 1){
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	@Override
	public boolean delete(Vehicle object) {
		String req = String.format(
				"DELETE FROM %s WHERE %s = ?", 
				VehicleDAO.TABLE_NAME, VehicleDAO.ID);
		
		
		try {
			PreparedStatement st = Connexion.getConnection().prepareStatement(req);
			
			st.setInt(1, object.getIdentifiant());
			
			if(st.executeUpdate() >= 1){
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	@Override
	public Vehicle findById(int id) {
		String req = String.format(
				"SELECT * FROM %s WHERE %s = ?",
				VehicleDAO.TABLE_NAME, VehicleDAO.ID);
		
		
		try {
			PreparedStatement st = Connexion.getConnection().prepareStatement(req);
			
			st.setInt(1, id);
			
			ResultSet rs = st.executeQuery();
				
			if(rs.next()){
				return this.cursorToVehicle(rs);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return null;
	}
	@Override
	public List<Vehicle> findAll() {
		String req = String.format("SELECT * FROM %s", VehicleDAO.TABLE_NAME);
		
		try {
			PreparedStatement st = Connexion.getConnection().prepareStatement(req);
			ResultSet rs = st.executeQuery();
			
			ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
			
			while (rs.next()) {
				vehicles.add(this.cursorToVehicle(rs));
			}
			
			return vehicles;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	
	private Vehicle cursorToVehicle(ResultSet resultSet){
		Vehicle vehicle = null;
		try {
			vehicle = new Vehicle(
					resultSet.getInt(VehicleDAO.ID), 
					resultSet.getString(VehicleDAO.BRAND), 
					resultSet.getString(VehicleDAO.MODEL),
					resultSet.getString(VehicleDAO.COLOR), 
					resultSet.getInt(VehicleDAO.YEAR),
					resultSet.getDouble(VehicleDAO.PRICE));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return vehicle;
	}

	
}
